// ---Dependencys
import { ReactElement } from 'react';
// ---Components
import GetApiCont from 'Cont/Axios/GetApiCont';
import PostApiCont from 'Cont/Axios/PostApiCont';
import PostVender from 'Cont/Axios/PostVender';

// ------------------------------------------ COMPONENT-----------------------------------------
export default function HomeCont(): ReactElement {
  // --- Const Hooks and States
  // ---- Main Methods
  return (
    <div>
      <PostVender />
    </div>
  );
}
