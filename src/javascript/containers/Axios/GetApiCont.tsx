import axios from "axios";
import { useState, useEffect } from "react";

export default function GetApiCont() {
  const [data, setData] = useState([]);
  useEffect(() => {
    const GetAxios = async () => {
      const urlGet = "http://localhost:8080/consultarVentas/2022-01-16";
      try {
        const respuesta = await axios(urlGet, { method: "GET" });
        const sell = respuesta.data.orders;
        console.log(sell);
        setData(sell);
        return sell;
      } catch (error) {
        console.log(error);
        console.log("no entra el url");
        return null;
      }
    };
    GetAxios();
  }, []);
  return (
    <div>
      {data.map((a, i) => (
        <ul>
          <li>
            Id Orden
            {a.id}
          </li>
          <li>
            Date
            {a.date}
          </li>
          <li>
            {a.productos.map((b:any, j:any) => (
              <ul>
                <li>{b.cod}</li>
                <li>{b.name}</li>
                <li>{b.tipo}</li>
                <li>
                  $
                  {b.price}
                </li>
              </ul>
            ))}
          </li>
          <li>
            Total Orden$
            {a.totalOrder}
          </li>
        </ul>
      ))}
    </div>
  );
}
