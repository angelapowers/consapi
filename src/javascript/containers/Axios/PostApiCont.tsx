import axios from "axios";
import { ChangeEvent, useState } from 'react';

interface CafeCat {
    cod: String;
    name: String;
    tipo: String;
    size: String;
    price: Number;
    category: String[];
  }
export default function PostApiCont() {
  const [datosCaf, setDatosCaf] = useState <CafeCat>({
    cod: '',
    name: '',
    tipo: '',
    size: '',
    price: 0,
    category: []
  });
  const handleInputChange = (reg:ChangeEvent<HTMLInputElement>) => {
    setDatosCaf({
      ...datosCaf,
      [reg.target.name]: reg.target.value
    });
  };
  const enviarDatos = (event:any) => {
    event.preventDefault();
    const urlPost = "http://localhost:8080/registraProductoCategoria";
    function postApi() {
      axios
        .post(urlPost, datosCaf)
        .then((res) => {
          if (res.status === 200) {
            console.log("realizado", res.data);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
    postApi();
  };
  return (
    <div>
      <h2>Registrar Datos</h2>
      <form action="Ingresar Datos" onSubmit={enviarDatos}>
        <input placeholder="Ingresar Codigo" type="text" name="cod" onChange={handleInputChange} />
        <br />
        <input placeholder="Ingresar Nombre" type="text" name="name" onChange={handleInputChange} />
        <br />
        <input placeholder="Ingresar tipo" type="text" name="tipo" onChange={handleInputChange} />
        <br />
        <input placeholder="Ingresar Tamaño" type="text" name="size" onChange={handleInputChange} />
        <br />
        <input placeholder="Ingresar Precio" type="Number" name="price" onChange={handleInputChange} />
        <br />
        <input placeholder="Ingresar Categoria" type="text" name="category" onChange={handleInputChange} />
        <br />
        <button type="submit">Enviar</button>
      </form>
    </div>
  );
}
