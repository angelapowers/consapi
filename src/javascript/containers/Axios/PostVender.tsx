/* eslint-disable no-array-constructor */
import axios from "axios";
import { ChangeEvent, useState, FormEvent } from "react";

interface DatosVenta {
  id: number;
  prod: String[];
  date: Date;
}
export default function PostVender() {
  const [ventas, setVentas] = useState<DatosVenta>({
    id: 0,
    prod: [],
    date: new Date(Date.now())
  });
  const handleInputChange = (reg: ChangeEvent<HTMLInputElement>) => {
    setVentas({
      ...ventas,
      [reg.target.name]: reg.target.value
    });
  };
  function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    const valius = Object.values(event.target);
    console.log(valius);
    const formElements = valius.filter((element) => isHTMLInput(element));
    console.log(formElements);
    const cleanFormElement = formElements.map((element: any) => (
      element.value
    ));
    console.log(cleanFormElement);
  }
  function isHTMLInput(o: any) {
    const isHtml = typeof HTMLElement === "object"
      ? o instanceof HTMLElement // DOM2
      : o
          && typeof o === "object"
          && o !== null
          && o.nodeType === 1
          && typeof o.nodeName === "string";
    return isHtml && o && o.type && o.type === "text";
  }
  function enviarDatos(datos: DatosVenta) {
    const urlPost = "http://localhost:8080/vender";
    function postApi() {
      axios
        .post(urlPost, datos)
        .then((res) => {
          if (res.status === 200) {
            console.log("realizado", res.data);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
    postApi();
  }
  return (
    <div>
      <h2>Registrar Datos</h2>
      <form action="Ingresar Datos" onSubmit={onSubmit}>
        <input placeholder="ID" type="Number" name="id" />
        <br />
        <input placeholder="Ingresar Producto" type="text" name="prod" />
        <br />
        <input placeholder="Ingresar Producto" type="text" name="prod" />
        <br />
        <input placeholder="Ingresar Producto" type="text" name="prod" />
        <br />
        <button type="submit">Enviar</button>
      </form>
    </div>
  );
}
